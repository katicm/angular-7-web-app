import { Component, OnInit, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { Racun } from "../../models/racun";
import { HttpClient } from "@angular/common/http";
import { RacunService } from "../../services/racun.service";
import {
  MatDialog,
  MatTableDataSource,
  MatPaginator,
  MatSort
} from "@angular/material";
import { RacunDialogComponent } from "../dialogs/racun-dialog/racun-dialog.component";

@Component({
  selector: "app-racun",
  templateUrl: "./racun.component.html",
  styleUrls: ["./racun.component.css"]
})
export class RacunComponent implements OnInit {
  displayedColumns = ["id", "datum", "nacin_placanja", "actions"];
  // dataSource: Observable<Racun[]>;

  dataSource: MatTableDataSource<Racun>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public racunService: RacunService
  ) {}

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this.racunService.getAllRacun().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      //ignoriši mala/velika slova pri sortiranju ali za id nemoj da prebacuješ u mala slova
      this.dataSource.sortingDataAccessor = (data, property) => {
        switch (property) {
          case "id":
            return data[property];
          default:
            return data[property].toLocaleLowerCase();
        }
      };

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  public openDialog(
    flag: number,
    id: number,
    datum: Date,
    nacin_placanja: string
  ) {
    const dialogRef = this.dialog.open(RacunDialogComponent, {
      data: { id: id, datum: datum, nacin_placanja: nacin_placanja }
    });
    dialogRef.componentInstance.flag = flag;
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) this.loadData();
    });
  }
}
