import { Component, OnInit, ViewChild } from "@angular/core";
import { Observable } from "rxjs";
import { Proizvodjac } from "../../models/proizvodjac";
import { HttpClient } from "@angular/common/http";
import {
  MatDialog,
  MatTableDataSource,
  MatPaginator,
  MatSort
} from "@angular/material";
import { ProizvodjacService } from "../../services/proizvodjac.service";
import { ProizvodjacDialogComponent } from "../dialogs/proizvodjac-dialog/proizvodjac-dialog.component";

@Component({
  selector: "app-proizvodjac",
  templateUrl: "./proizvodjac.component.html",
  styleUrls: ["./proizvodjac.component.css"]
})
export class ProizvodjacComponent implements OnInit {
  displayedColumns = ["id", "adresa", "naziv", "kontakt", "actions"];
  dataSource: MatTableDataSource<Proizvodjac>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public proizvodjacService: ProizvodjacService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this.proizvodjacService.getAllProizvodjac().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  public openDialog(
    flag: number,
    id: number,
    adresa: string,
    naziv: string,
    kontakt: string
  ) {
    const dialogRef = this.dialog.open(ProizvodjacDialogComponent, {
      data: { id: id, adresa: adresa, naziv: naziv, kontakt: kontakt }
    });
    dialogRef.componentInstance.flag = flag;
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) this.loadData();
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
  }
}
