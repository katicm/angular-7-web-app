import { Component, OnInit, Inject } from "@angular/core";
import { Racun } from "../../../models/racun";
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { StavkaRacuna } from "../../../models/stavkaRacuna";
import { StavkaRacunaService } from "../../../services/stavkaRacuna.service";
import { RacunService } from "../../../services/racun.service";
import { Proizvod } from "src/app/models/proizvod";
import { ProizvodService } from "src/app/services/proizvod.service";

@Component({
  selector: "app-stavka-racuna-dialog",
  templateUrl: "./stavka-racuna-dialog.component.html",
  styleUrls: ["./stavka-racuna-dialog.component.css"]
})
export class StavkaRacunaDialogComponent implements OnInit {
  racuni: Racun[];
  proizvodi: Proizvod[];
  public flag: number;

  constructor(
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<StavkaRacunaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: StavkaRacuna,
    public stavkaRacunaService: StavkaRacunaService,
    public racunService: RacunService,
    public proizvodService: ProizvodService
  ) {}

  ngOnInit() {
    this.racunService.getAllRacun().subscribe(racuni => (this.racuni = racuni));
    this.proizvodService
      .getAllProizvod()
      .subscribe(proizvodi => (this.proizvodi = proizvodi));
  }

  public compareTo(a, b) {
    return a.id == b.id;
  }

  public add(): void {
    this.data.id = -1;
    this.stavkaRacunaService.addStavkaRacuna(this.data);
    this.snackBar.open("Uspešno dodata stavka racuna", "U redu", {
      duration: 2500
    });
  }

  public update(): void {
    this.stavkaRacunaService.updateStavkaRacuna(this.data);
    this.snackBar.open("Uspešno modifikovana stavka racuna", "U redu", {
      duration: 2500
    });
  }

  public delete(): void {
    this.stavkaRacunaService.deleteStavkaRacuna(this.data.id);
    this.snackBar.open("Uspešno obrisana stavka racuna", "U redu", {
      duration: 2500
    });
  }

  public cancel(): void {
    this.dialogRef.close();
    this.snackBar.open("Odustali ste", "U redu", { duration: 2500 });
  }
}
