import { Component, OnInit, Inject } from "@angular/core";
import { Proizvodjac } from "../../../models/proizvodjac";
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Proizvod } from "../../../models/proizvod";
import { ProizvodService } from "../../../services/proizvod.service";
import { ProizvodjacService } from "../../../services/proizvodjac.service";

@Component({
  selector: "app-proizvod-dialog",
  templateUrl: "./proizvod-dialog.component.html",
  styleUrls: ["./proizvod-dialog.component.css"]
})
export class ProizvodDialogComponent implements OnInit {
  proizvodjaci: Proizvodjac[];
  public flag: number;

  constructor(
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ProizvodDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Proizvod,
    public proizvodService: ProizvodService,
    public proizvodjacService: ProizvodjacService
  ) {}

  ngOnInit() {
    this.proizvodjacService
      .getAllProizvodjac()
      .subscribe(proizvodjaci => (this.proizvodjaci = proizvodjaci));
  }

  compareTo(a, b) {
    return a.id == b.id;
  }

  onChange(proizvodjac) {
    this.data.proizvodjac = proizvodjac;
  }

  public add(): void {
    this.data.id = -1;
    this.proizvodService.addProizvod(this.data);
    this.snackBar.open("Uspešno dodata porudžbina", "U redu", {
      duration: 2500
    });
  }

  public update(): void {
    this.proizvodService.updateProizvod(this.data);
    this.snackBar.open("Uspešno modifikovana porudžbina", "U redu", {
      duration: 2500
    });
  }

  public delete(): void {
    this.proizvodService.deleteProizvod(this.data.id);
    this.snackBar.open("Uspešno obrisana porudžbina", "U redu", {
      duration: 2500
    });
  }

  public cancel(): void {
    this.dialogRef.close();
    this.snackBar.open("Odustali ste", "U redu", {
      duration: 1000
    });
  }
}
