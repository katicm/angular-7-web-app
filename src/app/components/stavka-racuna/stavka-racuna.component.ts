import { Component, OnInit, Input, ViewChild } from "@angular/core";
import {
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog
} from "@angular/material";
import { Proizvod } from "../../models/proizvod";
import { Racun } from "../../models/racun";
import { StavkaRacunaDialogComponent } from "../dialogs/stavka-racuna-dialog/stavka-racuna-dialog.component";
import { StavkaRacunaService } from "../../services/stavkaRacuna.service";
import { StavkaRacuna } from "../../models/stavkaRacuna";

@Component({
  selector: "app-stavka-racuna",
  templateUrl: "./stavka-racuna.component.html",
  styleUrls: ["./stavka-racuna.component.css"]
})
export class StavkaRacunaComponent implements OnInit {
  displayedColumns = [
    "id",
    "redniBroj",
    "kolicina",
    "jedinica_mere",
    "cena",
    "racun",
    "proizvod",
    "actions"
  ];
  dataSource: MatTableDataSource<StavkaRacuna>;

  @Input() selektovanaProizvod: Proizvod;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public stavkaRacunaService: StavkaRacunaService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    if (this.selektovanaProizvod.id) {
      this.loadData();
    }
  }

  public loadData() {
    this.stavkaRacunaService
      .getStavkeZaProizvod(this.selektovanaProizvod.id)
      .subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.filterPredicate = (data, filter: string) => {
          const accumulator = (currentTerm, key) => {
            return key === "proizvod"
              ? currentTerm + data.proizvod.naziv
              : currentTerm + data[key];
          };
          const dataStr = Object.keys(data)
            .reduce(accumulator, "")
            .toLowerCase();
          const transformedFilter = filter.trim().toLowerCase();
          return dataStr.indexOf(transformedFilter) !== -1;
        };
        //sortiranje po nazivu ugnježdenog objekta
        this.dataSource.sortingDataAccessor = (data, property) => {
          switch (property) {
            case "racun":
              return data.racun.nacin_placanja.toLocaleLowerCase();
            case "proizvod":
              return data.proizvod.naziv.toLocaleLowerCase();
            default:
              return data[property];
          }
        };

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  public openDialog(
    flag: number,
    id: number,
    redniBroj: number,
    kolicina: number,
    jedinica_mere: string,
    cena: number,
    racun: Racun,
    proizvod: Proizvod
  ) {
    const dialogRef = this.dialog.open(StavkaRacunaDialogComponent, {
      data: {
        i: id,
        id: id,
        redniBroj: redniBroj,
        kolicina: kolicina,
        jedinica_mere: jedinica_mere,
        cena: cena,
        racun: racun,
        proizvod: proizvod
      }
    });
    dialogRef.componentInstance.flag = flag;
    if (flag == 1)
      dialogRef.componentInstance.data.proizvod = this.selektovanaProizvod;

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) this.loadData();
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
  }
}
