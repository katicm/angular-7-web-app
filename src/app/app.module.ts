import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from "./app.component";
import { VoziloComponent } from "./components/primer-components/vozilo/vozilo.component";
import { AutomobilComponent } from "./components/primer-components/automobil/automobil.component";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import {
  MatButtonModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatExpansionModule,
  MatGridListModule,
  MatTableModule,
  MatToolbarModule,
  MatSelectModule,
  MatOptionModule,
  MatDialogModule,
  MatInputModule,
  MatSnackBarModule,
  MatNativeDateModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatSortModule
} from "@angular/material";

import { AboutComponent } from "./components/core/about/about.component";
import { HomeComponent } from "./components/core/home/home.component";
import { AuthorComponent } from "./components/core/author/author.component";
import { RacunComponent } from "./components/racun/racun.component";
import { ProizvodjacComponent } from "./components/proizvodjac/proizvodjac.component";
import { ProizvodComponent } from "./components/proizvod/proizvod.component";
import { StavkaRacunaComponent } from "./components/stavka-racuna/stavka-racuna.component";
import { HttpClientModule } from "@angular/common/http";
import { RacunService } from "./services/racun.service";
import { RacunDialogComponent } from "./components/dialogs/racun-dialog/racun-dialog.component";
import { FormsModule } from "@angular/forms";
import { ProizvodjacDialogComponent } from "./components/dialogs/proizvodjac-dialog/proizvodjac-dialog.component";
import { ProizvodjacService } from "./services/proizvodjac.service";
import { ProizvodDialogComponent } from "./components/dialogs/proizvod-dialog/proizvod-dialog.component";
import { ProizvodService } from "./services/proizvod.service";
import { StavkaRacunaDialogComponent } from "./components/dialogs/stavka-racuna-dialog/stavka-racuna-dialog.component";
import { StavkaRacunaService } from "./services/stavkaRacuna.service";

const Routes = [
  { path: "racun", component: RacunComponent },
  { path: "proizvodjac", component: ProizvodjacComponent },
  { path: "proizvod", component: ProizvodComponent },
  { path: "stavkaRacuna", component: StavkaRacunaComponent },
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "author", component: AuthorComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
  declarations: [
    AppComponent,
    VoziloComponent,
    AutomobilComponent,
    AboutComponent,
    HomeComponent,
    AuthorComponent,
    RacunComponent,
    ProizvodjacComponent,
    ProizvodComponent,
    StavkaRacunaComponent,
    RacunDialogComponent,
    ProizvodjacDialogComponent,
    ProizvodDialogComponent,
    StavkaRacunaDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(Routes),
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatExpansionModule,
    MatTableModule,
    MatToolbarModule,
    MatSelectModule,
    MatOptionModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDialogModule,
    MatInputModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule
  ],
  entryComponents: [
    RacunDialogComponent,
    ProizvodjacDialogComponent,
    ProizvodDialogComponent,
    StavkaRacunaDialogComponent
  ],
  providers: [
    RacunService,
    ProizvodjacService,
    ProizvodService,
    StavkaRacunaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
