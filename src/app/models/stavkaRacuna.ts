import { Proizvod } from "./proizvod";
import { Racun } from "./racun";

export class StavkaRacuna {
  id: number;
  redniBroj: number;
  kolicina: number;
  jedinica_mere: string;
  cena: number;
  racun: Racun;
  proizvod: Proizvod;
}
